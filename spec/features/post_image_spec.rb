require 'rails_helper'

RSpec.feature "PostImages", type: :feature do
  background do
    @user = create(:user)
  end

  scenario "is success" do
    file_path = Rails.root.join('spec', 'factories', 'test.png')
    
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: @user.password

    click_on "ログインする"
    
    visit user_path(@user)

    page.has_no_selector? "li"

    click_on "写真をアップロードする"

    attach_file "image_post_image", file_path
    
    click_on "送信する"

    visit user_path(@user)

    page.has_selector? "li"
  end

  # scenario "is deleted", js: true do
  #   file_path = Rails.root.join('spec', 'factories', 'test.png')
    
  #   visit root_path

  #   fill_in "email", with: @user.email
  #   fill_in "password", with: @user.password

  #   click_on "ログインする"
    
  #   visit user_path(@user)

  #   click_on "写真をアップロードする"

  #   attach_file "image_post_image", file_path
    
  #   click_on "送信する"

  #   visit user_path(@user)

  #   page.has_selector? "li"

  #   page.accept_confirm do
  #     click_on "posted-image-delete-1"
  #   end

  #   page.has_no_selector? "li"
  # end
end

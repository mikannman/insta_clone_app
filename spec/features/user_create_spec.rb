require 'rails_helper'

RSpec.feature "UserCreates", type: :feature do
  scenario "create user" do
    visit root_path
    
    click_on "登録する"
    expect(page).to have_button "登録する"

    fill_in "user_email", with: "example@example.com"
    fill_in "user_full_name", with: "sample taro"
    fill_in "user_user_name", with: "samle_taro"
    fill_in "user_password", with: "password"
    fill_in "user_password_confirmation", with: "password"

    click_on "登録する"
    expect(page).to have_content "sample taro"
    expect(page).to have_selector "div.alert"
  end

  scenario "create user failed" do
    visit root_path
    
    click_on "登録する"
    expect(page).to have_button "登録する"

    fill_in "user_email", with: "example@example.com"
    fill_in "user_full_name", with: ""
    fill_in "user_user_name", with: "samle_taro"
    fill_in "user_password", with: "password"
    fill_in "user_password_confirmation", with: "password"

    click_on "登録する"
    expect(page).to have_button "登録する"
  end
end

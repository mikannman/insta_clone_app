require 'rails_helper'

RSpec.feature "PasswordChanges", type: :feature do
  background do
    @user = create(:user)
  end

  scenario "changing is success" do
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: @user.password

    click_on "ログインする"
    
    visit edit_password_change_path(@user)

    fill_in "user_current_password", with: "password"
    fill_in "user_password", with: "hogehoge"
    fill_in "user_password_confirmation", with: "hogehoge"

    click_button "パスワードを変更"

    expect(page).to have_content "パスワードを変更しました"
  end

  scenario "changing is failure because it is not match" do
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: @user.password

    click_on "ログインする"
    
    visit edit_password_change_path(@user)

    fill_in "user_current_password", with: "password"
    fill_in "user_password", with: "hogehoge"
    fill_in "user_password_confirmation", with: "hoge"

    click_button "パスワードを変更"

    expect(page).to have_content "パスワードが変更できません"
  end

  scenario "changing is failure because it is blank" do
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: @user.password

    click_on "ログインする"
    
    visit edit_password_change_path(@user)

    fill_in "user_current_password", with: "password"
    fill_in "user_password", with: ""
    fill_in "user_password_confirmation", with: ""

    click_button "パスワードを変更"

    expect(page).to have_content "パスワードが変更できません"
  end
end

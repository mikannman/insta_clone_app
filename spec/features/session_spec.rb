require 'rails_helper'

RSpec.feature "Sessions", type: :feature do
  background do
    @user = create(:user)
  end

  scenario "login" do
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: @user.password

    click_on "ログインする"

    expect(page).to have_content "写真をアップロードする"
  end

  scenario "login failed" do
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: "sample"

    click_on "ログインする"

    expect(page).to_not have_content "写真をアップロードする"
  end

  scenario "logout" do
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: @user.password

    click_on "ログインする"

    expect(page).to have_content "写真をアップロードする"

    click_on "ログアウト"

    expect(page).to_not have_content "写真をアップロードする"
  end
end

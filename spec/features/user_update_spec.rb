require 'rails_helper'

RSpec.feature "UserUpdates", type: :feature do
  background do
    @user = create(:user)
  end

  scenario "no_change_updating is success" do
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: @user.password

    click_on "ログインする"

    visit edit_user_path(@user)

    click_on "送信する"

    expect(page).to have_content "プロフィールを更新しました"
  end

  scenario "updating is success" do
    visit root_path

    fill_in "email", with: @user.email
    fill_in "password", with: @user.password

    click_on "ログインする"
    
    visit edit_user_path(@user)

    fill_in "user_site_url", with: "https://www.google.com/"
    fill_in "user_phone_num", with: "0120-123-456"

    click_on "送信する"

    expect(page).to have_content "プロフィールを更新しました"
  end
end

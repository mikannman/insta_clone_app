require 'rails_helper'

RSpec.describe Relationship, type: :model do
  before do
    @user = create(:user)
    @other_user = create(:user)
    @relationship = Relationship.new(follower_id: @user.id,
            followed_id: @other_user.id)
  end

  example "should be valid" do
    expect(@relationship).to be_valid
  end

  example "should require a follower_id" do
    @relationship.follower_id = nil
    expect(@relationship).to_not be_valid
  end

  example "should require a followed_id" do
    @relationship.followed_id = nil
    expect(@relationship).to_not be_valid
  end
end

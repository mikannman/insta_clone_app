require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user = create(:user)
    @other_user = create(:user)
  end

  describe "user_model validation" do
    example "should be valid" do
      expect(@user).to be_valid
    end

    example "user_name should be presence" do
      @user.user_name = ""
      expect(@user).to_not be_valid
    end

    example "user_name should be unique" do
      @other_user.user_name = @user.user_name
      expect(@other_user).to_not be_valid
    end

    example "user_names are not case sensitive" do
      @other_user.user_name = @user.user_name.upcase
      expect(@other_user).to_not be_valid
    end

    example "name should not be too long" do
      @user.user_name = "a" * 22
      expect(@user).to_not be_valid
    end

    example "site_url is valid" do
      @user.site_url = "https://www.google.com/"
      expect(@user).to be_valid
    end

    example "site_url is not valid" do
      @user.site_url = "abab" * 4
      expect(@user).to_not be_valid
    end

    example "phone_num is valid" do
      @user.phone_num = "0120-123-456"
      expect(@user).to be_valid
    end

    example "phone_num is not valid" do
      @user.phone_num = "000"
      expect(@user).to_not be_valid
    end

    example "gender is valid" do
      @user.gender = 1
      expect(@user).to be_valid
      expect(@user.gender).to eq("man")
    end

    example "gender is not valid" do
      @user.phone_num = 5
      expect(@user).to_not be_valid
    end

    example "should follow and unfollow a user" do
      expect(@user.following?(@other_user)).to be_falsy
      @user.follow(@other_user)
      expect(@user.following?(@other_user)).to be_truthy
      expect(@other_user.followers.include?(@user)).to be_truthy
      @user.unfollow(@other_user)
      expect(@user.following?(@other_user)).to be_falsy
    end
  end

end

module RequestHelpers
  # テストユーザーとしてログインする
  def log_in_as(user, password: "password")
    post login_path, params: {
            email: user.email,
            password: password
    }
  end

  # テストユーザーがログイン中の場合はtrueを返す
  def is_logged_in?
    !session[:user_id].nil?
  end
end
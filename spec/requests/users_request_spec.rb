require 'rails_helper'

RSpec.describe "Users", type: :request do
  describe "operate user" do
    describe "creating user" do
      example "is sccess" do
        get signup_path
        expect do
          post signup_path, params: { user: {
                          full_name: "Example User",
                          user_name: "example_user",
                          email: "example@example.com",
                          password: "password",
                          password_confirmation: "password"
                        } }
        end.to change{ User.count }.by(1)
      end

      example "is failure" do
        get signup_path
        expect do
          post signup_path, params: { user: {
                          full_name: "",
                          user_name: "example_user",
                          email: "example@example.com",
                          password: "password",
                          password_confirmation: "password"
                        } }
        end.to change{ User.count }.by(0)
      end
    end
  end

  describe "require login" do
    before do
      @user = create(:user)
    end

    context "login" do
      before do
        log_in_as @user
      end

      example "new_path with login" do
        get new_user_path
        expect(response.status).to eq 200
      end

      example "show_path with login" do
        get user_path(@user)
        expect(response.status).to eq 200
      end

      example "edit_path with login" do
        get edit_user_path(@user)
        expect(response.status).to eq 200
      end
    end

    context "do not login" do
      example "new_path without login" do
        get new_user_path
        expect(response.status).to eq 200
      end

      example "show_path without login" do
        get user_path(@user)
        expect(response.status).to_not eq 200
      end

      example "edit_path without login" do
        get edit_user_path(@user)
        expect(response.status).to_not eq 200
      end
    end
  end

  describe "correct_user?" do
    before do
      @user = create(:user)
      @other_user = create(:user)
      log_in_as @user
    end

    example "is not" do
      get edit_user_path(@other_user)
      expect(response.status).to_not eq 200
    end
  end

  describe "updating user" do
    before do
      @user = create(:user)
      log_in_as @user
    end

    example "full_name change update" do
      get edit_user_path(@user)
      patch user_path(@user), params: { user: {
        full_name: "Change Name",
        user_name: @user.user_name,
        email: @user.email,
      } }
      expect(@user.reload.full_name).to eq "Change Name"
    end
  end
end

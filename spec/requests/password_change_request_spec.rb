require 'rails_helper'

RSpec.describe "PasswordChanges", type: :request do
  describe "changing password" do
    before do
      @user = create(:user)
      log_in_as @user
    end

    example "is success" do
      patch password_change_path(@user), params: { user: {
        current_password: "password",
        password: "hogehoge",
        password_confirmation: "hogehoge"
      } }
      expect(@user.reload.authenticate("hogehoge")).to be_truthy
    end

    example "is failure because it is too short" do
      patch password_change_path(@user), params: { user: {
        current_password: "password",
        password: "hoge",
        password_confirmation: "hoge"
      } }
      expect(@user.reload.authenticate("password")).to be_truthy
    end

    example "is failure because it is blank" do
      patch password_change_path(@user), params: { user: {
        current_password: "password",
        password: "",
        password_confirmation: ""
      } }
      expect(@user.reload.authenticate("password")).to be_truthy
    end
  end
end

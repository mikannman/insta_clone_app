FactoryBot.define do
  factory :user do
    full_name { Faker::Name.name }
    sequence(:user_name) { |n| "example_#{n}" }
    sequence(:email) { |n| "mail_#{n}@example.com" }
    password { "password" }
    password_confirmation { "password" }
  end
end

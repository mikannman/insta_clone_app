class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.integer :do_id
      t.references :user, null: false, foreign_key: true
      t.integer :image_id
      t.string :state

      t.timestamps
    end
  end
end

class AddColumnToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :site_url, :string
    add_column :users, :introduction, :text
    add_column :users, :phone_num, :string
    add_column :users, :gender, :integer # 1:男 2:女
  end
end

class ChangePasswordToUsers < ActiveRecord::Migration[6.0]
  def up
    change_column :users, :user_name, :string, null: true
    change_column :users, :password_digest, :string, null: true
  end
end

class ChangeColumnToUsers < ActiveRecord::Migration[6.0]
  def up
    change_column :users, :phone_num, :string, unique: true
    change_column :users, :gender, :integer, default: 0 # 1:男 2:女
  end
end

class AddFullNameColumnToUsers < ActiveRecord::Migration[6.0]
  def up
    add_column :users, :full_name, :string
    change_column :users, :full_name, :string, null: false
  end

  def down
    remove_column :users, :full_name
  end
end

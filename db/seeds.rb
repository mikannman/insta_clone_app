# ユーザー
User.create!(full_name: "Example User",
            user_name: "example_user",
            email: "sample@example.com",
            password: "password",
            password_confirmation: "password")

User.create!(full_name: "青木 友慈",
            user_name: "mikanman",
            email: "mikanndorobou@gmail.com",
            password: "password",
            password_confirmation: "password")

User.create!(full_name: "Taro User",
            user_name: "taro_user",
            email: "taro@example.com",
            password: "password",
            password_confirmation: "password")

User.create!(full_name: "Jiro User",
            user_name: "jiro_user",
            email: "jiro@example.com",
            password: "password",
            password_confirmation: "password")

20.times do |n|
  user_name = "example_#{n}"
  email = "mail_#{n}@example.com"
  User.create!(full_name: Faker::Name.name,
              user_name: user_name,
              email: email,
              password: "password",
              password_confirmation: "password")
end

# リレーションシップ
users = User.all
user = users.first
following = users[2..10]
followers = users[3..10]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
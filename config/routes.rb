Rails.application.routes.draw do
  root "static_pages#home"
  get "/signup", to: "users#new"
  post "/signup", to: "users#create"
  post "/login", to: "sessions#create"
  delete "/logout", to: "sessions#destroy"
  post "likes/:image_id", to: "likes#create", as: :likes
  delete "likes/:image_id", to: "likes#destroy", as: :like
  post "favorites/:image_id", to: "favorites#create", as: :favorites
  delete "favorites/:image_id", to: "favorites#destroy", as: :favorite

  get '/auth/:provider/callback', to: 'users#facebook_login', as: :auth_callback
  get '/auth/failure', to: 'users#auth_failure', as: :auth_failure

  resources :users do
    member do
      get :favorite
      get :following
      get :followers
    end
    collection do
      post :notification
      get :search
    end
  end
  resources :password_change, only: [:edit, :update]
  resources :images, only: [:new, :show, :create, :destroy] do
    collection do
      post :preview
    end
  end
  resources :comments, only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :facebook_login, only: [:edit, :update]
end

require_relative "boot"

require "rails/all"

Bundler.require(*Rails.groups)

module InstaCloneApp
  class Application < Rails::Application
    config.load_defaults 6.0

    config.timezone = "Tokyo"
    config.i18n.load_path +=
      Dir[Rails.root.join("config", "locales", "**", "*.{rb, yml}").to_s]
    config.i18n.default_locale = :ja

    config.generators do |g|
      g.skip_routes true
      g.helpers false
      g.assets false
      g.test_framework :rspec
      g.controller_specs false
      g.view_specs false
      g.helper_specs false
      g.request_specs false
    end

    config.action_view.embed_authenticity_token_in_remote_forms = true
  end
end

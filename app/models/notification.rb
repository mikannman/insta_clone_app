class Notification < ApplicationRecord
  belongs_to :user

  default_scope -> { order(created_at: :desc) }

  scope :five_notifications, ->(id) { where("user_id = :user_id", user_id: id).limit(5) }
end

class User < ApplicationRecord
  has_many :images, dependent: :destroy
  has_many :comments, through: :images, dependent: :destroy
  has_many :active_relationships, class_name: "Relationship",
          foreign_key: "follower_id",
          dependent: :destroy
  has_many :passive_relationships, class_name:  "Relationship",
          foreign_key: "followed_id",
          dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  has_many :likes, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :favorited_images, through: :favorites, source: :image
  has_many :notifications, dependent: :destroy
  
  before_save :downcase_email, :downcase_name

  # s3との連携
  has_one_attached :avatar
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
          uniqueness: { case_sensitive: false },
          format: { with: VALID_EMAIL_REGEX }

  validates :full_name, presence: true, length: { maximum: 50 }

  VALID_USER_NAME_AND_PASSWORD_REGEX = /\A\w+\z/
  validates :user_name, presence: true, length: { in: 3..20 },
          uniqueness: { case_sensitive: false },
          format: { with: VALID_USER_NAME_AND_PASSWORD_REGEX }

  has_secure_password
  validates :password, presence: true, length: { in: 6..12 },
          allow_nil: true,
          format: { with: VALID_USER_NAME_AND_PASSWORD_REGEX }

  VALID_URL_REGEX = /\A#{URI::regexp(%w(http https))}\z/
  validates :site_url, allow_nil: true,
          format: { with: VALID_URL_REGEX, allow_blank: true }

  VALID_PHONE_NUMBER_REGEX = /\A(((0(\d{1}[-(]?\d{4}|\d{2}[-(]?\d{3}|
    \d{3}[-(]?\d{2}|\d{4}[-(]?\d{1}|[5789]0[-(]?\d{4})[-)]?)|\d{1,4}\-?)\d{4}|0120[-(]?\d{3}[-)]?\d{3})\z/x
  validates :phone_num, uniqueness: true, allow_nil: true, allow_blank: true,
          format: { with: VALID_PHONE_NUMBER_REGEX, allow_blank: true }

  enum gender: { man: 1, woman: 2, other: 3 }

  scope :name_search, ->(word) { where("user_name LIKE :word OR full_name LIKE :word", word: "%#{word}%").limit(10) }

  # 渡された文字列のハッシュ値を返す
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                              BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # ユーザーをフォローする
  def follow(other_user)
    following << other_user
  end

  # ユーザーをフォロー解除する
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # 現在のユーザーがフォローしていたらtrueを返す
  def following?(other_user)
    following.include?(other_user)
  end

  # ユーザーのステータスフィードを返す
  def feed
    Image.feeds(id)
  end

  # はじめから６つの写真を返す
  def six_images(image_id)
    Image.six_images(id, image_id)
  end

  # はじめから１０個の通知を返す
  def five_notifications
    Notification.five_notifications(id)
  end

  # ユーザーが通知を見たかどうかを返す
  def unchecked_notifications?
    notifications.where(checked: false).any?
  end

  # ユーザーがいいねしているか返す
  def liked?(image)
    likes.exists?(image_id: image.id)
  end

  # ユーザーがお気に入りにしているか返す
  def favorited?(image)
    favorites.exists?(image_id: image.id)
  end

  # facebookログインに関するメソッド
  def self.from_omniauth(auth)
    user = User.where('email = ?', auth.info.email).first
    if user.blank?
       user = User.new
    end
    user.uid = auth.uid
    user.full_name = auth.info.name
    user.email = auth.info.email
    user.oauth_token = auth.credentials.token
    user.oauth_expires_at = Time.at(auth.credentials.expires_at)
    user
  end

  # メールアドレスを全て小文字にする
  private def downcase_email
    email.downcase! if email.present?
  end

  # 名前を全て小文字にする
  private def downcase_name
    user_name.downcase! if user_name.present?
  end
end

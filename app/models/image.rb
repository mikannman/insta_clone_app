class Image < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :favorites, dependent: :destroy
  
  default_scope -> { order(created_at: :desc) }

  following_ids = "SELECT followed_id FROM relationships
          WHERE follower_id = :user_id"
  scope :feeds, ->(id) { where("user_id IN (#{following_ids})
          OR user_id = :user_id", user_id: id) }
  scope :six_images, ->(id, image_id) { where("user_id = :user_id",
   user_id: id).where.not("id = :image_id", image_id: image_id).limit(6) }
          
  # s3との連携
  has_one_attached :post_image

  # 投稿されたコメントの最初の二つを取得
  def posted_comments
    Comment.where("image_id = :image_id", image_id: id).limit(2)
  end
end

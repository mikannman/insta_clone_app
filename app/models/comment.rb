class Comment < ApplicationRecord
  belongs_to :image

  validates :comment, presence: true

  default_scope -> { order(created_at: :desc) }
end

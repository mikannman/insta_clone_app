class SessionsController < ApplicationController
  def create
    @user = User.find_by(email: params[:email].downcase)
    if @user && @user.authenticate(params[:password])
      log_in @user
      redirect_to root_path
    else
      flash.now[:danger] = "メールアドレスかパスワードが間違っています"
      render template: "static_pages/home"
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end

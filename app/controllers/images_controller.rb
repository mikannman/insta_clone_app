require "tempfile"

class ImagesController < ApplicationController
  include Rails.application.routes.url_helpers

  before_action :logged_in_user
  before_action :correct_user, only: :destroy

  def new
    @image = Image.new
  end

  def show
    @image = Image.find(params[:id])
    @user = @image.user
  end

  def create
    image = @current_user.images.new(image_params)
    if image.save
      redirect_to image_path(image.id)
    else
      render "new"
    end
  end

  def destroy
    @image.destroy
    @user = User.find(params[:user_id])
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def preview
    raw_image = MiniMagick::Image.read(params[:file])
    resized_image = resize_with_crop(raw_image, 500, 500)
    @image = "data:image/png;base64,#{Base64.strict_encode64(resized_image.to_blob)}"
  end

  private def image_params
    params.require(:image).permit(:post_image)
  end

  private def correct_user
    @image = current_user.images.find_by(id: params[:id])
    redirect_to root_url if @image.nil?
  end

  private def resize_with_crop(img, w, h)
    w_original, h_original = [img[:width].to_f, img[:height].to_f]

    op_resize = ''

    if w_original * h < h_original * w
      op_resize = "#{w.to_i}x"
    else
      op_resize = "x#{h.to_i}"
    end

    img.combine_options do |i|
      i.resize(op_resize)
      i.gravity(:center)
      i.crop "#{w.to_i}x#{h.to_i}+0+0"
    end

    img
  end
end

class FavoritesController < ApplicationController
  before_action :logged_in_user

  def create
    current_user.favorites.create!(image_id: params[:image_id])
    @image = Image.find(params[:image_id])
    user = @image.user
    unless current_user == user
      user.notifications.create!(do_id: current_user.id, image_id: @image.id, state: "favorite")
    end
    respond_to do |format|
      format.html { redirect_to root_url }
      format.js
    end
  end

  def destroy
    @favorite = Favorite.find_by(user_id: current_user.id, image_id: params[:image_id])
    @image = Image.find(params[:image_id])
    @favorite.destroy
    respond_to do |format|
      format.html { redirect_to root_url }
      format.js
    end
  end
end

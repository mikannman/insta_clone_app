class PasswordChangeController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if (@user.authenticate(params[:user][:current_password]) &&
      params[:user][:password] == params[:user][:password_confirmation])
      if params[:user][:password].empty?
        flash.now[:danger] = "パスワードが変更できません"
        render "edit"
      elsif @user.update(user_params)
        flash[:success] = "パスワードを変更しました"
        redirect_to @user
      else
        flash.now[:danger] = "パスワードが変更できません"
        render "edit"
      end
    else
      flash.now[:danger] = "パスワードが変更できません"
      render "edit"
    end
  end
end

private def user_params
  params.require(:user).permit(:password, :password_confirmation)
end

class CommentsController < ApplicationController
  before_action :logged_in_user

  def create
    @image = Image.find(params[:comment][:image_id])
    @image.comments.create!(comment_params)
    @state = params[:comment][:state]
    user = @image.user
    unless current_user == user
      user.notifications.create!(do_id: current_user.id, image_id: @image.id, state: "comment")
    end
    respond_to do |format|
      format.html { redirect_to current_user }
      format.js
    end
  end

  def destroy
  end

  private def comment_params
    params.require(:comment).permit(:comment, :user_id)
  end
end

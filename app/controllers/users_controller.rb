class UsersController < ApplicationController
  before_action :logged_in_user, only: [
    :index, :show, :edit, :update, :destroy, :favorite, :following, :followers
  ]
  before_action :correct_user, only: [:edit, :update, :destroy, :favorite]

  def index
    @users = User.all
    @user = current_user
  end

  def show
    @user = User.find(params[:id])
    @images = @user.images
    @favorites = current_user.favorited_images
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "ようこそ#{@user.full_name}さん"
      redirect_to @user
    else
      render "new"
    end
  end

  def edit
  end

  def update
    if @user.update(user_params)
      if avatar = params[:user][:avatar]
        @user.avatar.attach(avatar)
      end
      flash[:success] = "プロフィールを更新しました"
      redirect_to @user
    else
      render "edit"
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "ユーザーを削除しました"
    redirect_to root_url
  end

  def favorite
    @user = User.find(params[:id])
    @favorites = @user.favorited_images
  end

  def notification
    if @notifications = current_user.notifications
      @notifications.each do |notification|
        notification.update(checked: true)
      end
    end
  end

  def search
    word = params[:word]
    @results = search_user(word)
  end

  def followers
    @user = User.find(params[:id])
    @followers = @user.followers.page(params[:page]).per(5)
  end

  def following
    @user = User.find(params[:id])
    @following = @user.following.page(params[:page]).per(5)
  end

  def facebook_login
    @user = User.from_omniauth(request.env["omniauth.auth"])
    result = @user.save(validate: false)
    if result
      log_in @user
      if @user.user_name.blank?
        redirect_to edit_facebook_login_path(@user)
      else
        redirect_to @user
      end
    else
      redirect_to auth_failure_path
    end
  end

  #認証に失敗した際の処理
  def auth_failure
    @user = User.new
    redirect_to root_url
  end

  private def user_params
    params.require(:user).permit(:email, :full_name,
            :user_name, :password, :password_confirmation,
            :site_url, :introduction, :phone_num,
            :gender)
  end

  # ユーザーネームかフルネームからユーザーを検索する(10人まで)
  private def search_user(word)
    User.name_search(word)
  end
end

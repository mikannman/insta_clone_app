class FacebookLoginController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user

  def edit
    @user = User.find(params[:id])
  end

  def update
    if @user.update(user_params)
      redirect_to @user
    else
      render "edit"
    end
  end

  private def user_params
    params.require(:user).permit(:user_name, :password, :password_confirmation)
  end
end

class LikesController < ApplicationController
  before_action :logged_in_user
  
  def create
    current_user.likes.create(image_id: params[:image_id])
    @image = Image.find(params[:image_id])
    respond_to do |format|
      format.html { redirect_to root_url }
      format.js
    end
  end

  def destroy
    @like = Like.find_by(user_id: current_user.id, image_id: params[:image_id])
    @image = Image.find(params[:image_id])
    @like.destroy
    respond_to do |format|
      format.html { redirect_to root_url }
      format.js
    end
  end
end

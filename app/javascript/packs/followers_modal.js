$(document).on("turbolinks:load", () => {
  $("#followers").on("click", () => {
    let $fadeLayer = $("#fade-layer")
    $fadeLayer.css({
      "visibility": "visible",
    });
    $.ajax({
      url: $("#followers").data("request-url"),
      type: "GET",
    });
    $("#followers-modal").css("visibility", "visible");
    $fadeLayer.on("click", () => {
      $fadeLayer.css("visibility", "hidden");
      $("#followers-modal").css("visibility", "hidden");
    });
  });
});

$(document).on("turbolinks:load", () => {
  $(".bell").on("click", () => {
    $("#fade-clear-notifications-layer").css({
      "visibility": "visible",
    });
    let modal = $(".notification-modal-wrapper")
    modal.css("visibility", "visible");
    $("#fade-clear-notifications-layer").on("click", () => {
      $("#fade-clear-notifications-layer").css("visibility", "hidden");
      modal.css("visibility", "hidden");
    });
  });
});

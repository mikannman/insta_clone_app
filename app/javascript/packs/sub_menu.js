function hideScrollBar() {
	$("body").css("overflow", "hidden");
}

function appearScrollBar() {
	$("body").css("overflow", "auto");
}

$(document).on("turbolinks:load", () => {
  $(".cog").on("click", () => {
    let pos = $(window).scrollTop();
    $("#fade-layer").css({
      "visibility": "visible",
      "top": pos
    });
    $(".sub-menu-wrapper").css("visibility", "visible");
    $(window).on("wheel", (e) => {
      e.preventDefault();
    });
    hideScrollBar();
    $("#fade-layer").on("click" ,() => {
      $("#fade-layer").css("visibility", "hidden");
      $(".sub-menu-wrapper").css("visibility", "hidden");
      $(window).off("wheel");
      appearScrollBar();
    });
  });
});

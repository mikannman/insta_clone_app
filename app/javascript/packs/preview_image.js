$(document).on("turbolinks:load", () => {
  $("#upload-image-input").on("change", () => {
    let data = new FormData();
    data.append('file', $("#upload-image-input").prop('files')[0]);

    $.ajax({
      url: $("#upload-image-input").data("request-url"),
      type: "POST",
      data: data,
      contentType: false,
      processData: false,
    });
    $("#preview-image-area").css("visibility", "visible");
  });
});

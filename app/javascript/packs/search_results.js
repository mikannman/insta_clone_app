$(document).on("turbolinks:load", () => {
  $(".search-text-box").on("change", () => {
    $("#fade-clear-layer").css({
      "visibility": "visible",
    });
    let $form = $(".header-search");
    $.ajax({
      url: $form.attr("action"),
      type: $form.attr("method"),
      data: $form.serialize()
    });
    $("#search-results-area").css("visibility", "visible");
    $("#fade-clear-layer").on("click", () => {
      $("#fade-clear-layer").css("visibility", "hidden");
      $("#search-results-area").css("visibility", "hidden");
    });
  });
});

$(document).on("turbolinks:load", () => {
  $("#following").on("click", () => {
    let $fadeLayer = $("#fade-layer")
    $fadeLayer.css({
      "visibility": "visible",
    });
    $.ajax({
      url: $("#following").data("request-url"),
      type: "GET",
    });
    $("#following-modal").css("visibility", "visible");
    $fadeLayer.on("click", () => {
      $fadeLayer.css("visibility", "hidden");
      $("#following-modal").css("visibility", "hidden");
    });
  });
});

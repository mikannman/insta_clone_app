function hideScrollBar() {
	$("body").css("overflow", "hidden");
}

function appearScrollBar() {
	$("body").css("overflow", "auto");
}

$(document).on("turbolinks:load", () => {
  $(".show-posted-image").on("click", (event) => {
    let pos = $(window).scrollTop();
    $("#fade-layer").css({
      "visibility": "visible",
      "top": pos
    });
    let image = $(event.currentTarget).children(".show-page-modal")
    image.css("visibility", "visible");
    $(window).on("wheel", (e) => {
      e.preventDefault();
    });
    hideScrollBar();
    $("#fade-layer").on("click", () => {
      $("#fade-layer").css("visibility", "hidden");
      image.css("visibility", "hidden");
      $(window).off("wheel");
      appearScrollBar();
    });
  });
});
